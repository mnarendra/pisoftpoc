var app = angular.module('app',['ui.router']);
app.config(function($stateProvider, $urlRouterProvider){
    $urlRouterProvider.otherwise("/");
       $stateProvider
           .state('login', {
               url: '/',
               templateUrl: 'login.html',
               controller: 'loginController'
               
           })
            .state('forgot', {
               url: '/',
               templateUrl: 'reset-password.html',
               controller: 'resetController'
               
           })
           .state('register', {
               url: '/register',
               templateUrl: 'newcustomer.html',
               controller: 'regisController'
               
           })
           .state('home', {
               url: '/home',
               templateUrl: 'home.html',
               controller: 'homeController'
             
           })
            .state('profile', {
               url: '/profile',
               templateUrl: 'profile.html',
               controller: 'profileController'
               
           });
    
});

app.controller('regisController',function($scope,$location){
// var sqlite3 = require('sqlite3').verbose();
// var db = new sqlite3.Database('naru.db'); // naru is the name of my database

// db.serialize(function () {
//    db.run("CREATE TABLE Customer_reg(id integer primary key,first_name TEXT,last_name TEXT,email TEXT,password password TEXT,phoneNumber integer,Gender Text)");

//   db.run("INSERT INTO Test1 VALUES (?, ?, ?, ?, ?, ?,?)");
//   //db.run("INSERT INTO Test1 VALUES (?, ?, ?)", [101, 'naren2', 'hyd']);
//  // db.run("INSERT INTO Test1 VALUES (?, ?, ?)", [102, 'naren1', 'hyd']);

//   // db.each("SELECT cname FROM Test1", function (err, row) {
//   //   console.log(row);
//   // });
// // db.serialize(function () {
// //   db.each("SELECT * FROM Test", function (err, row) {
// //     console.log(row);
// //   });
// // });
// // db.run("DELETE from Test1 where cname='naren1'");
// //db.run("update Test1 set cname='Narendra' where cname='naren2'");

// //Table-2
// // db.run("CREATE TABLE company(id INT,name TEXT,salary BIGINT)");
// // db.run("INSERT INTO company VALUES (?, ?, ?)", [101, 'ramu', 2000]);
// // db.run("INSERT INTO company VALUES (?, ?, ?)", [102, 'prasad', 4000]);
// // db.run("INSERT INTO company VALUES (?, ?, ?)", [103, 'ravi', 12000]);
// // db.run("INSERT INTO company VALUES (?, ?, ?)", [104, 'ajay', 11000]);
// // db.run("INSERT INTO company VALUES (?, ?, ?)", [105, 'chakri', 15000]);
// // db.each("SELECT * FROM company where id in(select id from company where salary>10000)", function (err, row) {
// //     console.log(row);
// //    });
// db.run("drop table company");
// db.each("SELECT * FROM company where id in(select id from company where salary>10000)", function (err, row) {
//      console.log(row);
//     });

// });
// db.close();

});

//mylogincontroller
app.controller('loginController',['$scope','$location',function($scope,$location){
$scope.loginVal=function(){
$location.path("/home");
}
}]);
 app.controller('profileController',['$scope','$location',function($scope,$location){

}]);
 app.controller('homeController',['$scope','$location',function($scope,$location){
 $scope.logout=function(){
 
 $location.path("/");
}
}]);




// var sqlite3 = require('sqlite3').verbose();
// var db = new sqlite3.Database('naru1.db'); // naru1 is the name of my database

//   db.serialize(function () {

//   // db.run("CREATE TABLE Test1 (cid, cname, cadd)");
//   // db.run("INSERT INTO Test1 VALUES (?, ?, ?)", [100, 'naren3', 'hyd']);
//   // db.run("INSERT INTO Test1 VALUES (?, ?, ?)", [101, 'naren2', 'hyd']);
//   // db.run("INSERT INTO Test1 VALUES (?, ?, ?)", [102, 'naren1', 'hyd']);

//   // db.each("SELECT cname FROM Test1", function (err, row) {
//   //   console.log(row);
//   // });
//   // db.run("update Customer set cname='Narendra123' where cname='naren2'");
//   // db.run("DELETE from Customer where cname='naren1'");

//   db.each("SELECT cname FROM Customer ", function (err, row) {
//     console.log("cname:"+row.cname);
//     // console.log("Your Table data is retrieved Here:"+"cid:"+row.cid+"cname:"+row.cname+"cadd:"+row.cadd);
//   });
// });
// db.close();
